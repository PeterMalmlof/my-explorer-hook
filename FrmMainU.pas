unit FrmMainU;

interface

uses
  Windows,  Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DateUtils, StrUtils;

const
  MyExplorerHook = 'MyExplorerHook.dll';  // WHookInt.pas

const
  WM_HOOK_MSG  = WM_USER + 300; // Used by the Hook to return Messages

//------------------------------------------------------------------------------
// Special Key Codes returned from the Remote Controler through Virtual Codes
// You need to Manage these hooking the WM_KEYDOWN message.
// This object will also return them as Codes (That you should not act on)
// This will make it transparent if Keys comes from Keyboard or Remote
// Note: Return, Left, Right, Up, Down, numbers (0..9), and Clear will not
// be returned by Remote handler at all, they will be sent directly as normal
// virtual KeyCodes by Windows. (VK_RETURN etc.) You need to handle them.
//------------------------------------------------------------------------------
const
  VK_VOLUME_MUTE          = 173; // (AD) Volume Mute key       Kb
  VK_VOLUME_DOWN          = 174; // (AE) Volume Down key       Kb
  VK_VOLUME_UP            = 175; // (AF) Volume Up key         Kb
  VK_MEDIA_NEXT_TRACK     = 176; // (B0) Next Track key        Kb
  VK_MEDIA_PREV_TRACK     = 177; // (B1) Previous Track key    Kb
  VK_MEDIA_STOP           = 178; // (B2) Stop Media key        Kb
  VK_MEDIA_PLAY_PAUSE     = 179; // (B3) Play/Pause Media key  Kb
  VK_BROWSER_BACK         = 166; // (A6) Back, Escape button   Rem
  VK_LAUNCH_MEDIA_SELECT  = 181; // (B5)                       Kb

type
  TFrmMain = class(TForm)
    Memo1: TMemo;
    BtnSetHook: TButton;
    BtnClearHook: TButton;
    procedure BtnSetHookClick(Sender: TObject);
    procedure BtnClearHookClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    FHookSet: Boolean;

    procedure EnableButtons;
    procedure Log(Line : string);

    procedure OnMyMessage(var Msg: TMsg; var Handled: Boolean);
  public

  end;

var
  FrmMain: TFrmMain;

//------------------------------------------------------------------------------
//  External Hook.ddl functions
//------------------------------------------------------------------------------
//  Set Hook
//------------------------------------------------------------------------------
function SetHook(WinHandle: HWND; MsgToSend: Integer): integer; stdcall;
  external MyExplorerHook;
//------------------------------------------------------------------------------
//  Free Hook
//------------------------------------------------------------------------------
function FreeHook: integer; stdcall; external MyExplorerHook;

//------------------------------------------------------------------------------
//
//                              IMPLEMENTATION
//
//------------------------------------------------------------------------------
implementation

{$R *.DFM}

//------------------------------------------------------------------------------
// Return Text String from a WIndows error code
//------------------------------------------------------------------------------
function GetErrorString(dw : DWORD): string;
var
  MsgBuf : Array [0..1023] of Char;
begin
  // Format the Message

  Windows.FormatMessage(
        FORMAT_MESSAGE_FROM_SYSTEM or
        FORMAT_MESSAGE_IGNORE_INSERTS,
        Nil,
        dw,
        0,
        MsgBuf,
        sizeof(MsgBuf),
        nil );

  result := MsgBuf;
  result := AnsiLeftStr(result, Length(result) - 1);
end;
//------------------------------------------------------------------------------
//  Application OnMessage
//------------------------------------------------------------------------------
procedure TFrmMain.OnMyMessage(var Msg: TMsg; var Handled: Boolean);
begin
  //
  case Msg.message of

    WM_KEYDOWN :
      begin
        // Application KEY_DOWN messages

        case Msg.wParam of
          VK_VOLUME_MUTE         : Log('Internal: Volume Mute key');
          VK_VOLUME_DOWN         : Log('Internal: Volume Down key');
          VK_VOLUME_UP           : Log('Internal: Volume Up key');
          VK_MEDIA_NEXT_TRACK    : Log('Internal: Next Track key');
          VK_MEDIA_PREV_TRACK    : Log('Internal: Previous Track key ');
          VK_MEDIA_STOP          : Log('Internal: Stop Media key');
          VK_MEDIA_PLAY_PAUSE    : Log('Internal: Play/Pause Media key');
          VK_BROWSER_BACK        : Log('Internal: Back, Escape button');
          VK_LAUNCH_MEDIA_SELECT : Log('Internal: Media Select');
        end;
      end;

    // Messages Posted by the Hook

    WM_HOOK_MSG :
      begin
        case Msg.wParam of
          0 : Log('Hook: Installed');

          VK_VOLUME_MUTE         : Log('Hook VK: Volume Mute key');
          VK_VOLUME_DOWN         : Log('Hook VK: Volume Down key');
          VK_VOLUME_UP           : Log('Hook VK: Volume Up key');
          VK_MEDIA_NEXT_TRACK    : Log('Hook VK: Next Track key');
          VK_MEDIA_PREV_TRACK    : Log('Hook VK: Previous Track key ');
          VK_MEDIA_STOP          : Log('Hook VK: Stop Media key');
          VK_BROWSER_BACK        : Log('Hook VK: Back, Escape button');
          VK_LAUNCH_MEDIA_SELECT : Log('Hook VK: Media Select');

          VK_MEDIA_PLAY_PAUSE : Log('Hook RAW: Play/Pause key');

        else
          Log('Hook: ' + IntToStr(Msg.wParam) + '/' + IntToStr(Msg.lParam));
        end;
      end;
  end;

  inherited;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TFrmMain.Log(Line : string);
var
  T  : TDateTime;
begin
  T := SysUtils.Time;

  Memo1.Lines.Append(SysUtils.TimeToStr(T) + '.' + SysUtils.Format('%.3d',
        [DateUtils.MilliSecondOf(T)]) + ' ' + Line);
end;
//------------------------------------------------------------------------------
//  Enable Buttons
//------------------------------------------------------------------------------
procedure TFrmMain.EnableButtons;
begin
  BtnSetHook.Enabled   := (not FHookSet);
  BtnClearHook.Enabled := FHookSet;
end;
//------------------------------------------------------------------------------
// Set Hook Buttom
//------------------------------------------------------------------------------
procedure TFrmMain.BtnSetHookClick(Sender: TObject);
var
  res : integer;
begin
  // Set Hook and give it the Message Id for returning messages
   
  res := SetHook(Handle, WM_HOOK_MSG);
  case res of
    0 : Log('Hook Set Sucessfully');
    1 : Log('Error: MapFile is already Mapped');
    2 : Log('Error: MapFile Already exists');
    3 : Log('Error: Could Not Create MapFile');
    4 : Log('Error: Could Not Create MapFile for other reason');
    5 : Log('Error: Couldnt Map to Address Space');
  else
    Log('SetHook Error: ' + IntToStr(res));
  end;

  if (res <> 0) then
    Log('Last Error: ' + GetErrorString(GetLastError));

  FHookSet := (res = 0);
  EnableButtons;
end;
//------------------------------------------------------------------------------
// Free Hook Buttom
//------------------------------------------------------------------------------
procedure TFrmMain.BtnClearHookClick(Sender: TObject);
var
  res : integer;
begin
  res := FreeHook;
  case res of
    0 : Log('Hook Freed Sucessfully');
    1 : Log('Error: MapFile not Mapped');
    2 : Log('Error: Could not UnHook Hook');
    3 : Log('Error: Couldnt UnMap the MapFile');
    4 : Log('Error: Couldnt Close the MapFile');
  else
    Log('FreeHook Error: ' + IntToStr(res));
  end;

  if (res <> 0) then
    Log('Last Error: ' + GetErrorString(GetLastError));

  FHookSet := not (res = 0);
  EnableButtons;
end;
//------------------------------------------------------------------------------
//  Create Form
//------------------------------------------------------------------------------
procedure TFrmMain.FormCreate(Sender: TObject);
begin
  FHookSet := false;

  EnableButtons;
  Memo1.Clear;

  Application.OnMessage := OnMyMessage;
end;
//------------------------------------------------------------------------------
//  Destroy Form
//------------------------------------------------------------------------------
procedure TFrmMain.FormDestroy(Sender: TObject);
begin
  // Free Hook whatever state its in

  FreeHook;
end;

end.

