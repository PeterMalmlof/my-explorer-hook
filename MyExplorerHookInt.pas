unit MyExplorerHookInt;

interface

uses
  Windows,
  Messages,
  SysUtils;

  // External Functions

  // Set Hook used by the application wanting the Hook

  function SetHook(WinHandle: HWND; MsgToSend: Integer): integer;
            stdcall; export;

  // Free Hook, used by caller application

  function FreeHook: integer;
            stdcall; export;

  // Message Filter Function, called by the Hook Chain

  function MsgFilterFunc(Code: Integer; wParam, lParam: Longint): Longint
            stdcall; export;

implementation

// DemoApp: FrmMainU.pas

//------------------------------------------------------------------------------
//
//                                  MEMORY MAP
//
//------------------------------------------------------------------------------
//  These functions are used by both Caller and Message Filter, they
//  dont use any attributes in common, they are free functions
//------------------------------------------------------------------------------
//  CreateMMF     Create Memory File
//    OpenMMF     Open the created Memory File
//      MapMMF    Map File inte my Address Space
//      UnMapMMF  Unmap File
//    CloseMMF    Close File
//------------------------------------------------------------------------------
//  CreateFileMapping function creates unnamed file-mapping object
//  for the specified file.
//------------------------------------------------------------------------------
function CreateMMF(Name: string; Size: Integer;out Res : integer): THandle;
begin

  // CreateFileMapping function creates a named or unnamed file-mapping
  // object for the specified file

  result := Windows.CreateFileMapping(
    $FFFFFFFF,      // Identifies the file from which to create a mapping object
    nil,            // Pointer to a SECURITY_ATTRIBUTES structure
    PAGE_READWRITE, // Gives read-write access to the committed region of pages
    0,              // high-order 32 bits of the maximum size
    Size,           // low-order  32 bits of the maximum size
    PChar(Name));   // name of the mapping object (no \)
  if (result = 0) then
    begin
      res := 2; // Could not create MapFile
    end
  else
    begin
      res := 0; // MapFile was created

      // If the function succeeds, the return value is a handle to the
      // file-mapping object. If the object existed before the function
      // call, the GetLastError function returns ERROR_ALREADY_EXISTS, 
      // and the return value is a valid handle to the existing file-mapping
      // object (with its current size, not the new specified size. 
      // If the mapping object did not exist, GetLastError returns zero.

      if (GetLastError = ERROR_ALREADY_EXISTS) then
        begin
          // NOTE: If DDL aborts with a File Mapped there is no
          //       way (that I know about) to Close it. It will stay
          //       opened until Windows is restarted. Closing is based
          //       on reference count internally in Windows
          
          CloseHandle(result);
          result := 0;
          Res    := 1; // MapFile already existed
        end;
    end;
end;
//------------------------------------------------------------------------------
// The OpenFileMapping function opens a named file-mapping object.
//------------------------------------------------------------------------------
function OpenMMF(Name: string): THandle;
begin
  // The OpenFileMapping function opens a named file-mapping object.
  // Read-write access. The target file-mapping object must have been
  // created with PAGE_READWRITE protection. A read-write view of the file
  // is mapped.
  // The return value is an open handle to the specified file-mapping object.

  Result := OpenFileMapping(FILE_MAP_ALL_ACCESS, False, PChar(Name));
end;
//------------------------------------------------------------------------------
// Map File into the Calling Process Address Space
//------------------------------------------------------------------------------
function MapMMF(MMFHandle: THandle): Pointer;
begin
  // The MapViewOfFile function maps a view of a file into the address
  // space of the calling process

  Result := MapViewOfFile(MMFHandle, FILE_MAP_ALL_ACCESS,
    0,  // High-order 32 bits of the file offset where mapping is to begin
    0,  // Low-order  32 bits of the file offset where mapping is to begin
    0); // Number of bytes of the file to map (0=entire file)

  // If the function succeeds, the return value is the starting address
  // of the mapped view
end;
//------------------------------------------------------------------------------
//  The UnmapViewOfFile function unmaps a mapped view of a file
//  from the calling process's address space.
//------------------------------------------------------------------------------
function UnMapMMF(P: Pointer): Boolean;
begin
  Result := UnmapViewOfFile(P);
end;
//------------------------------------------------------------------------------
// Close Memory MapFile
//------------------------------------------------------------------------------
function CloseMMF(MMFHandle: THandle): Boolean;
begin
  Result := CloseHandle(MMFHandle);
end;

//------------------------------------------------------------------------------
// Special Key Codes (wParam in WM_KEYDOWN) that is send by Keyboard to
// the Form having keyboard focus.
// They are also sent from the Remote Controller.
// These Messages should be removed from Message Chain and sent separatly
// to the Hooking Application. If not they will come twise to the Application
// (both internally and from the Hook) if the Application has focus
//
// Note: Return, Left, Right, Up, Down, numbers (0..9), and Clear will not
// be returned by Remote handler at all, they will be sent directly as normal
// virtual KeyCodes by Windows. (VK_RETURN etc.). There is no solution for
// this, we cant take them and kill them, other applications wont like that
//
// Note: Mute, Volume Up/Down will be received by Windows anyway and affect
//       the Soundcard. Thay are also sent to Volume Dialog.
//
// Note: VK_LAUNCH_MEDIA_SELECT will not be killed also, it will Launch
//       preferred Media Application anyway
//------------------------------------------------------------------------------
const
  VK_VOLUME_MUTE          = 173; // (AD) Volume Mute key       Kb
  VK_VOLUME_DOWN          = 174; // (AE) Volume Down key       Kb
  VK_VOLUME_UP            = 175; // (AF) Volume Up key         Kb
  VK_MEDIA_NEXT_TRACK     = 176; // (B0) Next Track key        Kb
  VK_MEDIA_PREV_TRACK     = 177; // (B1) Previous Track key    Kb
  VK_MEDIA_STOP           = 178; // (B2) Stop Media key        Kb
  VK_MEDIA_PLAY_PAUSE     = 179; // (B3) Play/Pause Media key  Kb
  VK_LAUNCH_MEDIA_SELECT  = 181; // (B5)                       Kb
  VK_BROWSER_BACK         = 166; // (A6) Back, Escape button   Rem

//------------------------------------------------------------------------------
//  Messages Sent from the Hook to caller Application is the MsgToSend
//  specified when the SetHook is called. The same wParam codes
//  are sent on that message as above
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//
//                                    HOOK
//
//------------------------------------------------------------------------------
type TPMsg = ^TMsg;
//------------------------------------------------------------------------------
// MemoryFile Data structure
//------------------------------------------------------------------------------
type
  PMMFData = ^TMMFData;
  TMMFData = record
    NextHook       : HHOOK;    // Next Hook in Chain
    WinHandle      : HWND;     // Callers Main Window (to receive Messages)
    MsgToSend      : Integer;  // Message Id to send to caller
  end;

const MMFName = 'MyExplorerHook';  // Memory File FileName

//------------------------------------------------------------------------------
//
//                                MESSAGE FILTER
//
//------------------------------------------------------------------------------
//  Message Filter Function
//   GetMsgProc (
//    Code   : Integer Specifies whether the hook procedure must process the
//                     message. If code is HC_ACTION, the hook procedure must
//                     process the message. If code is less than zero, the
//                     hook procedure must pass the message to the
//                     CallNextHookEx function without further processing
//                     and should return the value returned by CallNextHookEx.
//    wParam : longInt Specifies whether the message has been removed from
//                     the queue. This parameter can be one of the following
//                     values:
//    lParam : longInt Points to an MSG structure that contains details about 
//                     the message
//    result : longint The return value should be zero
//------------------------------------------------------------------------------
function MsgFilterFunc(Code : Integer; wParam, lParam : Longint): Longint;
var
  MMFH : THandle;  // MapFile Handle (only Valid here)
  MMFD : PMMFData; // MapFile Data   (only Valid here)
  Kill : boolean;  // True if Message should be removed
begin
  Result := 0;

  MMFH := OpenMMF(MMFName);
  if (MMFH <> 0) then
    begin
      MMFD := MapMMF(MMFH);
      if Assigned(MMFD) then
        begin
          if (Code < 0) or (wParam = PM_NOREMOVE) then
            begin
              // The CallNextHookEx function passes the hook information to the
              // next hook procedure in the current hook chain and returns
              // Its result

              Result := CallNextHookEx(MMFD.NextHook, Code, wParam, lParam);
            end
          else
            begin
              Kill := false; // Dont Kill by default

              // This Message should be processed

              // Handle KeyBoard Play Functions

              with TMsg(Pointer(lParam)^) do
                begin

                  case Message of
                    // Remote Controler AND Keyboard will send some
                    // Messages as KeyBoard VK_KEYDOWN messages
                    // We need to Send them as new Codes.
                    // NOT THE SAME, that will start an infinitive loop
                    // If the Application has the focus, they will arrive there
                    // also as WM_KEYDOWN and VK_MEDIA... messages

                    WM_KEYDOWN :
                      begin
                        // Post these Messages to Caller Application's Window
                        // And Kill them so they wont be received twice by
                        // an Application having KeyBoard Focus
                        
                        Kill := true;
                        case wParam of
                          VK_VOLUME_MUTE,
                          VK_VOLUME_DOWN,
                          VK_VOLUME_UP,
                          VK_MEDIA_NEXT_TRACK,
                          VK_MEDIA_PREV_TRACK,
                          VK_MEDIA_STOP,
                          VK_MEDIA_PLAY_PAUSE,
                          VK_LAUNCH_MEDIA_SELECT,
                          VK_BROWSER_BACK :
                            begin
                              PostMessage(MMFD.WinHandle,
                                          MMFD.MsgToSend, wParam, 0);
                            end;
                        else
                          Kill := false;
                        end;{case wParam}

                      end;{WM_KEYDOWN}
                  end;{case Message}
                end;{with}

              // Remove Message if necessary

              if Kill then TPMsg(lParam)^.message := WM_NULL;

              // Call NextHook

              Result := CallNextHookEx(MMFD.NextHook, Code, wParam, lParam)
            end;

          // UnMap Map File

          UnMapMMF(MMFD);
        end;

      // CLose Map File

      CloseMMF(MMFH);
    end;
end;
//------------------------------------------------------------------------------
//  These three functions are used by the Application Setting The Hook
//  These are the only functions allowed to use MMFHandle or MMFData
//------------------------------------------------------------------------------
var MMFHandle : THandle;   // Memory Map File Handle (Assigned if Hook exists)
    MMFData   : PMMFData;  // Pointer to Memory Map File Data (if mapped)
//------------------------------------------------------------------------------
//  The SetWindowsHookEx function installs an application-defined
//  hook procedure into a hook chain. This is called by the hooking
//  Application and only that knows about the MMFHandle and MMFData
//
//  WH_GETMESSAGE Installs a hook procedure that monitors messages
//  posted to a message queue.
//  For more information, see the GetMsgProc hook procedure.
//------------------------------------------------------------------------------
//  Internal: UnMap and CLose Memory File (used by SetHook and FreeHook only)
//------------------------------------------------------------------------------
function UnMapAndCloseMMF: integer;
begin
  if (not UnMapMMF(MMFData)) then
    begin
      result := 1; // Couldnt UnMap the MapFile
    end
  else
    begin
      MMFData := nil;
      if (not CloseMMF(MMFHandle)) then
        begin
          result := 2; // Couldnt Close the MapFile
        end
      else
        begin
          MMFHandle := 0;
          Result    := 0;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Set Hook
//------------------------------------------------------------------------------
function SetHook(WinHandle: HWND; MsgToSend: Integer): integer; stdcall;
var
  res : integer;
begin
  // Set Hook only if the Handle don't exist and isn't mapped already

  if Assigned(MMFData) or (MMFHandle <> 0) then
    begin
      result := 1; // MapFile is already Mapped
    end
  else
    begin
      // Create the Map File

      MMFHandle := CreateMMF(MMFName, SizeOf(TMMFData), res);
      if (MMFHandle = 0) then
        begin
          case res of
            1 : result := 2;  // MapFile Already exists
            2 : result := 3;  // Could Not Create MapFile
          else
            result := 4; // Could Not Create MapFile for other reason
          end;
        end
      else
        begin
          // Map Into My Address Space

          MMFData := MapMMF(MMFHandle);
          if (not Assigned(MMFData)) then
            begin
              result := 5; // Couldnt Map to Address Space

              // CLose MapFile and set it invalid

              CloseMMF(MMFHandle);
              MMFHandle := 0;
            end
          else
            begin
              // Set The Callers Window Handle in Data and the Message Id
              // To Send to its Caller

              MMFData.WinHandle := WinHandle;
              MMFData.MsgToSend := MsgToSend;

              // Install The Hook int the Hook Chain

              MMFData.NextHook  := SetWindowsHookEx(
                WH_GETMESSAGE, // Monitors messages posted to a message queue
                MsgFilterFunc, // Address To Hook Procedure
                HInstance,     // DLL containing the hook procedure
                0);            // Associate with all existing threads
              if (MMFData.NextHook = 0) then
                begin
                  result := 1; // Install Hook Didnt work

                  // Unmap MapFile and Close it

                  UnMapAndCloseMMF;
                end
              else
                begin
                  // We successfully Installed Hook, we leave MapFile
                  // opened until we Free Hook

                  Result := 0;

                  // Then post the return message to caller

                  PostMessage(WinHandle,MsgToSend, 0, 0);
                end;
            end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  The UnhookWindowsHookEx function removes the hook procedure installed
//  in a hook chain by the SetWindowsHookEx function.
//------------------------------------------------------------------------------
function FreeHook: integer; stdcall;
var
  res : integer;
begin
  // The MapFile Handle and MapFile Data must be Assigned

  if (not Assigned(MMFData)) or (MMFHandle = 0) then
    begin
      result := 1; // MapFile not Mapped
    end
  else
    begin
      // Unhook the Hook from the Hook Chain

      if (not UnHookWindowsHookEx(MMFData^.NextHook)) then
        begin
          result := 2; // Could not UnHook Hook

          // We leave MapFile Mapped and open (for no big reason)
        end
      else
        begin
          // UnMapp and Close Map File. If the refcount to the MapFile is
          // zero it will be deleted also.
          // Note: if the dll crashed the refcount wont go down to zero
          // and the next SetHook (with this name) wont succeed until
          // windows is restarted

          res := UnMapAndCloseMMF;
          if (res <> 0) then
            begin
              case res of
                1 : result := 3;  // Couldnt UnMap the MapFile
                2 : result := 4;  // Couldnt Close the MapFile
              else
                result := 5;
              end;
            end
          else
            result := 0;
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
initialization
  begin
    MMFHandle := 0;
    MMFData   := nil;
  end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
finalization
  FreeHook;
end.
