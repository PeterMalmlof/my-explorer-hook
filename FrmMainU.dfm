object FrmMain: TFrmMain
  Left = 422
  Top = 771
  Width = 442
  Height = 242
  Caption = 'FrmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    434
    208)
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 434
    Height = 169
    Align = alTop
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    TabOrder = 0
    WantReturns = False
  end
  object BtnSetHook: TButton
    Left = 7
    Top = 176
    Width = 75
    Height = 25
    Caption = 'BtnSetHook'
    TabOrder = 1
    OnClick = BtnSetHookClick
  end
  object BtnClearHook: TButton
    Left = 88
    Top = 176
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'BtnClearHook'
    TabOrder = 2
    OnClick = BtnClearHookClick
  end
end
